{
  gROOT->ProcessLine(".L mstools.C++");
  TChain ch("h1");
  //PATH TO RECONSTRUCTED MUON PARTICLE GUN ROOT FILES////////////
  ch.Add("../root/13_pgun_1500_140311_head_fiTQun.root");
  ////////////////////////////////////////////////////////////////
  mstools ms(&ch);
  ms.fillhistos();
  
  //HISTOGRAMS//
  // hp    - fractional difference between true and reconstructed momentum
  // hdir  - angle between true and reconstructed direction
  // hpos  - distance between true and reconstructed vertex
  // h2p   - fractional momentum difference vs true momentum
  // h2dir - angle difference vs true momentum
  // h2pos - position difference vs true momentum
  
}
